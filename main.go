package main

import (
	"bytes"
	"database/sql"
	"flag"
	"fmt"
	"github.com/bwmarrin/discordgo"
	_ "github.com/mattn/go-sqlite3"
	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"
	"image"
	"image/png"
	"log"
	"os"
	"strings"
	"sync"
)

type Config struct {
	serverAddress   string
	localpart       string
	password        string
	uploadComponent string
}

const helpText = `This is a very beta Discord bridge!
Commands:
- register [DISCORD TOKEN]: register with the bridge & connect
  (add -force to the end of the command to reregister)
- connect: connect to Discord manually
- help: view this help text`

var config *Config
var database *sql.DB
var sendChan chan stanza.Packet
var discordSessions map[string]*discordgo.Session = make(map[string]*discordgo.Session)
var discordSessionsLock sync.Mutex

func readyHandler(jid string, s *discordgo.Session, m *discordgo.Ready) {
	adminMsg(jid, fmt.Sprintf("Logged in to Discord as %s#%s.", m.User.Username, m.User.Discriminator))
}

func resumedHandler(jid string, s *discordgo.Session, m *discordgo.Resumed) {
	adminMsg(jid, "Resumed a previous Discord session.")
}

func disconnectHandler(jid string, s *discordgo.Session, m *discordgo.Disconnect) {
	adminMsg(jid, "Disconnected from Discord.")
}

type DiscordUserInformation struct {
	discord_id string
	name       string
	avatar     *image.Image
}

func getUserAvatar(db *sql.DB, s *discordgo.Session, discord_id string) {
	log.Printf("requesting avatar for %s", discord_id)
	image, e := s.UserAvatar(discord_id)
	if e != nil {
		log.Printf("%s avatar fetch failed: %v", discord_id, e)
		return
	}
	var buf bytes.Buffer
	e = png.Encode(buf, image)
	if e != nil {
		log.Printf("%s png encode failed: %v", discord_id, e)
		return
	}
	intid = discordUserIdToInternalId(db, discord_id)
	if e != nil {
		log.Printf("%s has no database entry? %v", e)
		return
	}
	_, e = db.Exec("INSERT INTO avatar_data (for_id, image) VALUES (?, ?) ON CONFLICT (for_id) DO UPDATE SET image = excluded.image", intid, buf.Bytes())
	if e != nil {
		log.Printf("%s db insert failed: %v", discord_id, e)
		return
	}
	log.Printf("fetched avatar for %s", discord_id)
}

func discordUserIdToInternalId(db *sql.DB, discord_id string) (id int64, e error) {
	row := db.QueryRow("SELECT id FROM discord_users WHERE discord_id = ?", discord_id)
	e = row.Scan(&id)
}

func insertUserInformation(db *sql.DB, user *discordgo.User) (id int64, e error) {
	if len(user.Username) < 1 || len(user.Discriminator) < 1 {
		e = fmt.Errorf("user object passed to insertUserInformation is useless")
		return
	}
	name := fmt.Sprintf("%s#%s", user.Username, user.Discriminator)
	info.name = name
	var result sql.Result
	result, e = db.Exec("INSERT INTO discord_users (discord_id, name) VALUES (?, ?) ON CONFLICT (discord_id) DO UPDATE SET name = excluded.name", discord_id, name)
	if e != nil {
		e = fmt.Errorf("inserting discord user %s: %w", discord_id, e)
		return
	}
	uid, e = result.LastInsertId()
	if e != nil {
		e = fmt.Errorf("getting insert id: %w", e)
		return
	}
}

func getUserInformation(db *sql.DB, s *discordgo.Session, discord_id string) (info DiscordUserInformation, e error) {
start:
	row := db.QueryRow("SELECT id, name FROM users WHERE discord_id = ?", discord_id)
	var uid int64
	e = row.Scan(&uid, &info.name)
	if e == sql.ErrNoRows {
		e = nil
		// Block to request the user information, because it's kinda
		// needed and doing this asynchronously is an Interesting idea.
		log.Printf("requesting user information for %s", discord_id)
		var user *discordgo.User // this is stupid
		user, e = s.User(discord_id)
		if e != nil {
			e = fmt.Errorf("requesting user information for %s: %w", discord_id, e)
			return
		}
		_, e = insertUserInformation(db, user)
		if e != nil {
			return
		}
		goto start
	} else {
		return
	}
	info.discord_id = discord_id
	var blob []byte
	row = db.QueryRow("SELECT image FROM avatar_data WHERE for_id = ?", uid)
	e = row.Scan(&blob)
	if e == sql.ErrNoRows {
		e = nil
		go getUserAvatar(db, s, discord_id)
	} else {
		return
	}
	if len(blob) > 0 {
		reader := bytes.NewReader(blob)
		var image image.Image
		image, e = png.Decode(reader)
		if e != nil {
			e = fmt.Errorf("decoding %s avatar: %w", discord_id, e)
			return
		}
		info.avatar = &image
	}
	return
}

func startDiscordSession(uid int64, jid string, token string) {
	log.Printf("starting discord session for %v", jid)
	adminMsg(jid, "Connecting to Discord...")
	defer discordSessionsLock.Unlock()
	discordSessionsLock.Lock()
	if discordSessions[jid] != nil {
		discordSessions[jid].Close()
	}
	discordSessions[jid] = nil
	sess, err := discordgo.New(token)
	if err != nil {
		log.Printf("error creating session: %v", err)
		adminMsg(jid, fmt.Sprintf("Error creating Discord session: %v", err))
		return
	}
	sess.ShouldReconnectOnError = true
	sess.AddHandler(func(s *discordgo.Session, m *discordgo.Ready) {
		readyHandler(jid, s, m)
	})
	sess.AddHandler(func(s *discordgo.Session, m *discordgo.Resumed) {
		resumedHandler(jid, s, m)
	})
	sess.AddHandler(func(s *discordgo.Session, m *discordgo.Disconnect) {
		disconnectHandler(jid, s, m)
	})
	discordSessions[jid] = sess
	err = sess.Open()
	if err != nil {
		discordSessions[jid] = nil
		log.Printf("error opening session: %v", err)
		adminMsg(jid, fmt.Sprintf("Error opening Discord session: %v", err))
		return
	}
}

func loadConfig(db *sql.DB) (*Config, error) {
	cfg := &Config{}
	row := db.QueryRow("SELECT address, component_name, shared_secret, upload_component_name FROM configuration WHERE rev = 1")
	err := row.Scan(&cfg.serverAddress, &cfg.localpart, &cfg.password, &cfg.uploadComponent)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func getUserId(db *sql.DB, jid string) (id *int64, e error) {
	row := db.QueryRow("SELECT id FROM users WHERE jid = ?", jid)
	var temp int64
	e = row.Scan(&temp)
	if e == sql.ErrNoRows {
		e = nil
	} else if e == nil {
		id = &temp
	}
	return
}

func getUserToken(db *sql.DB, uid int64) (token string, e error) {
	row := db.QueryRow("SELECT discord_token FROM users WHERE id = ?", uid)
	e = row.Scan(&token)
	return
}

func registerUser(db *sql.DB, jid string, discordtoken string) (uid int64, e error) {
	result, e := db.Exec("INSERT INTO users (jid, discord_token) VALUES (?, ?) ON CONFLICT (jid) DO UPDATE SET discord_token = excluded.discord_token", jid, discordtoken)
	if e != nil {
		return
	}
	uid, e = result.LastInsertId()
	return
}

func onXmppError(e error) {
	log.Fatalf("xmpp error: %v", e)
	os.Exit(1)
}

func onXmppEvent(e xmpp.Event) error {
	log.Printf("state changed: %v", e)
	return nil
}

func splitjid(jid string) (user string, localpart string, resource string) {
	at := strings.IndexRune(jid, '@')
	if at == -1 {
		log.Printf("bogus jid to split: %s", jid)
		return
	}
	user = jid[:at]
	slash := strings.IndexRune(jid, '/')
	end := len(jid)
	if slash != -1 {
		end = slash
		resource = jid[slash+1:]
	}
	localpart = jid[at+1 : end]
	return
}

func stripResource(jid string) string {
	slash := strings.IndexRune(jid, '/')
	if slash != -1 {
		return jid[:slash]
	} else {
		return jid
	}
}

func ouruser(jid string) (string, error) {
	user, localpart, _ := splitjid(jid)
	if localpart != config.localpart {
		err := fmt.Errorf("incoming stanza addressed to %v!", localpart)
		return "", err
	} else {
		return user, nil
	}
}

func adminjid() string {
	return fmt.Sprintf("admin@%s/admin", config.localpart)
}

func adminMsg(jid string, body string) {
	message := stanza.NewMessage(stanza.Attrs{
		From: adminjid(),
		To:   jid,
		Lang: "en",
	})
	message.Body = body
	sendChan <- message
}

func handleAdminCommand(from string, command string) error {
	if command == "" {
		panic("handleAdminCommand called with zero-length command")
	}
	parts := strings.Split(command, " ")
	uid, err := getUserId(database, from)
	if err != nil {
		return fmt.Errorf("getting user id: %w", err)
	}
	switch parts[0] {
	case "register":
		if len(parts) < 2 || len(parts[1]) < 5 {
			adminMsg(from, "Syntax: `register [DISCORD TOKEN] (-force)`")
			return nil
		} else {
			if uid != nil && len(parts) == 2 {
				adminMsg(from, "You're already registered! Add ` -force` to the end of the command to re-register.")
				return nil
			}
			uid, err := registerUser(database, from, parts[1])
			if err != nil {
				log.Printf("registration failed! %v", err)
				adminMsg(from, fmt.Sprintf("error: %v", err))
				return nil
			}
			adminMsg(from, "Registration succeeded.")
			go startDiscordSession(uid, from, parts[1])
		}
		break
	case "connect":
		if uid == nil {
			adminMsg(from, "You aren't registered.")
			return nil
		} else {
			token, err := getUserToken(database, *uid)
			if err != nil {
				log.Printf("failed to get user token: %v", err)
				adminMsg(from, "Database error!")
				return nil
			}
			go startDiscordSession(*uid, from, token)
		}
		break
	case "help":
		adminMsg(from, helpText)
		break
	default:
		adminMsg(from, fmt.Sprintf("unknown command: %s", command))
		adminMsg(from, "Try `help` for a list of commands.")
	}
	return nil
}

func makeMessageError(msg stanza.Message, err stanza.Err) stanza.Message {
	if err.Code == 0 {
		// I haven't the foggiest where this idea comes from, but
		// you have to set one of these for it to want to serialize
		// the error element. What?
		err.Code = 1
	}
	return stanza.Message{
		Error: err,
		Attrs: stanza.Attrs{
			From: msg.To,
			To:   msg.From,
			Id:   msg.Id,
			Type: "error",
		},
	}
}

func onXmppMessage(s xmpp.Sender, p stanza.Packet) {
	msg, ok := p.(stanza.Message)
	if !ok {
		log.Printf("ignoring dodgy packet: %T", p)
		return
	}
	from := stripResource(msg.From)
	to, err := ouruser(msg.To)
	if err != nil {
		log.Printf("%v", err)
		return
	}
	if msg.Body == "" {
		return
	}
	log.Printf("%v -> %v: %v", from, to, msg.Body)
	if to == "admin" {
		err := handleAdminCommand(from, msg.Body)
		if err != nil {
			log.Printf("error handling admin command: %v", err)
		}
	} else {
		log.Printf("unknown user %v", to)
		err := makeMessageError(msg, stanza.Err{
			Type:   stanza.ErrorTypeModify,
			Text:   "no such user",
			Reason: "item-not-found",
		})
		sendChan <- err
	}
}

func main() {
	dbpath := flag.String("database", "./data.sqlite3", "path to the database")
	flag.Parse()

	log.Printf("using database: %s\n", *dbpath)
	db, err := sql.Open("sqlite3", *dbpath)
	if err != nil {
		log.Fatalf("opening db: %v", err)
	}
	cfg, err := loadConfig(db)
	if err != nil {
		log.Fatalf("reading config: %v", err)
	}
	config = cfg
	database = db
	log.Printf("configuration loaded")
	xmppcfg := xmpp.ComponentOptions{
		TransportConfiguration: xmpp.TransportConfiguration{
			Address: cfg.serverAddress,
		},
		Domain:   cfg.localpart,
		Secret:   cfg.password,
		Name:     "go-discord-xmpp",
		Category: "gateway",
		Type:     "discord",
	}
	router := xmpp.NewRouter()
	router.HandleFunc("message", onXmppMessage)
	sendChan = make(chan stanza.Packet, 16)
	component, err := xmpp.NewComponent(xmppcfg, router, onXmppError)
	if err != nil {
		log.Fatalf("component creation failed: %v", err)
	}
	component.SetHandler(onXmppEvent)
	err = component.Connect()
	if err != nil {
		log.Fatalf("component connection failed: %v", err)
	}
	log.Printf("component started")
	for {
		pkt := <-sendChan
		err = component.Send(pkt)
		if err != nil {
			log.Fatalf("xmpp packet sending failed: %v", err)
		}
	}
}

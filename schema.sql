CREATE TABLE configuration (
       rev INT PRIMARY KEY,
       address VARCHAR NOT NULL,
       component_name VARCHAR NOT NULL,
       shared_secret VARCHAR NOT NULL,
       upload_component_name VARCHAR NOT NULL
);

CREATE TABLE users (
       id INTEGER PRIMARY KEY,
       jid VARCHAR UNIQUE NOT NULL,
       discord_token VARCHAR
);

CREATE TABLE discord_users (
       id INTEGER PRIMARY KEY,
       discord_id VARCHAR UNIQUE NOT NULL,
       name VARCHAR
);

CREATE TABLE avatar_data (
       for_id INTEGER UNIQUE NOT NULL REFERENCES discord_users,
       image BLOB NOT NULL
);

-- Maintains a mapping of Discord <-> XMPP message IDs.
--
-- When a message is sent, this table is populated with `waiting_for_ack`
-- set to true, and `xmpp_id` set to the message ID of the XMPP message.
--
-- When a message is received, this table is consulted. If there's already
-- an entry:
--  * if `waiting_for_ack` is true, it's set to false. If the message is a DM
--    (which will be known from having consulted the `channel_memberships`
--    table earlier), an XEP-0184 message delivery receipt is sent to the
--    JIDs specified in the aforementioned table. Otherwise, it's processed
--    as usual (to satisfy the MUC local echo requirement).
--  * otherwise, the message is dropped on the floor (to prevent duplicate
--    messages on the XMPP side)
-- If no entry exists, one is created with `waiting_for_ack` sent to false,
-- and `xmpp_id` set to a new UUID.
CREATE TABLE message_id_map (
       discord_id VARCHAR UNIQUE NOT NULL,
       xmpp_id VARCHAR UNIQUE NOT NULL,
       discord_channel_id VARCHAR NOT NULL,
       -- If we've just sent a message to a Discord channel,
       -- set to true.
       waiting_for_ack BOOLEAN NOT NULL DEFAULT false
);

-- Used to lookup which XMPP JIDs should receive a Discord message.
-- Discord has multiple different types of 'channels': some represent
-- 1-to-1 communication, in which case 'resource' is set to null
-- and all incoming messages get directed at the barejid,
-- and others represent actual group communications, in which case
-- the user's jid + `resource` represents the fulljid joined to the MUC.
--
-- When a new Discord message arrives, this table is first consulted
-- to determine where it goes. If no entries are present:
--   * if there's an entry in the muc_metadata table matching the
--     channel ID, stop.
--   * otherwise, look up the channel:
--   * if it's a DM channel, an entry is added to this table,
--     the user is sent a presence subscription request from the
--     recipient (so that they know their nickname), and the message
--     is sent.
--   * if it's a group DM or guild channel, an entry is added to the
--     muc_metadata table.
CREATE TABLE channel_memberships (
       id INTEGER PRIMARY KEY,
       user_id INT NOT NULL REFERENCES users,
       resource VARCHAR, -- Null if this is a DM channel
       discord_channel_id VARCHAR NOT NULL,
       UNIQUE(discord_channel_id, user_id, resource)
);

-- Keeps track of the Discord user IDs joined to a guild ID.
-- Also used for Group DM channels, where the guild ID is simply set to
-- the ID of the Group DM channel.
CREATE TABLE guild_discord_memberships (
       guild_id VARCHAR NOT NULL,
       discord_user_id VARCHAR NOT NULL,
       UNIQUE(guild_id, discord_user_id)
);

-- Keeps track of the name of a guild ID.
-- Also used for Group DM channels, where the name "DM" is inserted.
CREATE TABLE guild_names (
       guild_id VARCHAR PRIMARY KEY,
       name VARCHAR NOT NULL,
);

-- Stores metadata about any Discord channel that is bridged through
-- as a MUC on the XMPP side (so, guild channels and group DMs)
--
-- This gets populated and used:
--  * on a disco#info or MUC join request from a client
--  * on an incoming message for a channel ID we haven't seen yet
CREATE TABLE muc_metadata (
       channel_id VARCHAR PRIMARY KEY,
       guild_id VARCHAR NOT NULL,
       name VARCHAR NOT NULL,
       topic VARCHAR NOT NULL
);

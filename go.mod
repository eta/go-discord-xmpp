module theta.eu.org/go-discord-xmpp

go 1.14

require (
	github.com/bwmarrin/discordgo v0.20.3
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	gosrc.io/xmpp v0.5.1
)
